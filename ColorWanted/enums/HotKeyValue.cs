﻿namespace ColorWanted.enums
{
    /// <summary>
    /// 全局快捷键值定义
    /// </summary>
    enum HotKeyValue
    {
        /// <summary>
        /// 切换显示模式
        /// </summary>
        SwitchMode = 0xF00001,
        /// <summary>
        /// 复制RGB颜色值
        /// </summary>
        CopyRgbColor,
        /// <summary>
        /// 复制十六进制颜色值
        /// </summary>
        CopyHexColor,
        /// <summary>
        /// 显示预览窗口，隐藏鼠标
        /// </summary>
        ShowPreview,
        /// <summary>
        /// 显示调色板
        /// </summary>
        ShowColorPicker,
        /// <summary>
        /// 绘制预览窗口
        /// </summary>
        DrawPreview
    }
}
